importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/0385f5473832610fdaf9.js",
    "revision": "18e50723c25e0b171899bd6a36a501fa"
  },
  {
    "url": "/_nuxt/19be6c349a6ccdb688c7.js",
    "revision": "d74980dc4d81a947871874840a2b553b"
  },
  {
    "url": "/_nuxt/2184eaf70378e8247fa5.js",
    "revision": "c41a3d484d0ae35e2ebbc350a2236c05"
  },
  {
    "url": "/_nuxt/26b97eccc65329c90323.js",
    "revision": "f24df0b4efc1b05a851eceae73166413"
  },
  {
    "url": "/_nuxt/a30e3391ef79971a2543.js",
    "revision": "bdde35ef3da2073232a995770e604ccc"
  },
  {
    "url": "/_nuxt/bebedc06efbdf80e0f61.js",
    "revision": "6c4c280335e3829d77bf01dc11ace9fa"
  },
  {
    "url": "/_nuxt/de342255891a4a8fb93f.js",
    "revision": "26bd46530bcd7bf3e20a670c8ae9f75d"
  },
  {
    "url": "/_nuxt/e5c7d935696b2c640a15.js",
    "revision": "cb3ae1d8feccd13ddc6f5be014095d70"
  },
  {
    "url": "/_nuxt/f123e4154ba227e95983.js",
    "revision": "2dd52341e4a014271ea5d3d6fb5f1906"
  },
  {
    "url": "/_nuxt/f85d16f053267a15fc08.js",
    "revision": "fa2cbe834c06cd91ef84de1b4c902b8c"
  }
], {
  "cacheId": "web_psych_assess",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
